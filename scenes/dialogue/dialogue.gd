extends CanvasLayer

@onready var speech = $Background/MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/Speech
@onready var character_name_label = $Background/MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/CharacterName
@onready var buttons = $Background/MarginContainer/HBoxContainer/MarginContainer/VBoxContainer/Buttons
@onready var avatar = $Background/MarginContainer/HBoxContainer/Avatar

@export var action: String
@export var text: Array[String]
@export var character_name: String
@export var go_to: CanvasLayer
@export_enum("livre", "hache", "corde", "fleur", "jouet","CHIEF_KNEW","FORESTER_KNEW","TOMB_KNEW","AXE_KNEW") var show_on_milestone: String
@export_enum("livre", "hache", "corde", "fleur", "jouet","CHIEF_KNEW","FORESTER_KNEW","TOMB_KNEW","AXE_KNEW") var hide_on_milestone: String
@export var char_by_second: float = 0.05
@export var avatar_texture: Texture2D
@export var has_bye_button: bool = true

var tween

signal done
signal goes_to

func _ready():
	visible = false
	layer = 1
	character_name_label.text = character_name
	avatar.texture = avatar_texture

func play():
	# Remove all buttons
	for button in buttons.get_children():
		buttons.remove_child(button)
		button.queue_free()

	# Init button
	if go_to != null:
		var button = Button.new()
		button.text = "BACK"
		buttons.add_child(button)
		buttons.move_child(button, 0)
		button.connect("pressed", _on_go_back)

	for dialogue in get_children():
		if dialogue.is_in_group("dialogue"):
			# Check milestones
			if dialogue.show_on_milestone != "":
				if Globals.milestones[dialogue.show_on_milestone] == false:
					continue

			if dialogue.hide_on_milestone != "":
				if Globals.milestones[dialogue.hide_on_milestone] == true:
					continue

			var button = Button.new()
			button.text = dialogue.action
			buttons.add_child(button)
			buttons.move_child(button, 0)
			button.pressed.connect(runDialogue.bind(dialogue, button))
			dialogue.connect("done", _on_dialogue_done)
			dialogue.connect("goes_to", _on_dialogue_goes_to)
			dialogue.visible = false
			dialogue.layer = layer + 1
	if has_bye_button:
		add_bye_button()

	visible = true
	buttons.visible = false
	if text.size() > 0:
		speech.text = text.pick_random()
	if tween != null:
		tween.stop()
	speech.visible_ratio = 0
	var char = speech.get_total_character_count()
	var time = char * char_by_second
	tween = get_tree().create_tween()
	tween.connect("finished", _on_tween_finished)
	tween.tween_property(speech, "visible_ratio", 1, time)

func _process(delta):
	if Input.is_action_just_pressed("ui_accept") && tween != null && visible:
		tween.stop()
		speech.visible_ratio = 1
		buttons.visible = true

func _on_bye_pressed():
	emit_signal("done")
	visible = false
	layer = 1

func runDialogue(dialogue, button):
	dialogue.play()
	button.release_focus()

func _on_dialogue_done():
	emit_signal("done")
	visible = false
	layer = 1

func _on_tween_finished():
	buttons.visible = true

func _on_go_back():
	emit_signal("goes_to", go_to)
	visible = false
	layer = 1

func _on_dialogue_goes_to(destination):
	if destination != self:
		emit_signal("goes_to", destination)
		visible = false
		layer = 1

func add_bye_button():
	var button = Button.new()
	button.text = "GOODBYE"
	buttons.add_child(button)
	button.connect("pressed", _on_bye_pressed)
