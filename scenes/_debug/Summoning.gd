extends Node2D

@onready var player = $TileMap/Player
@onready var summoning = $Summoning
@onready var inventaire = $Summoning/Inventaire

# Called when the node enters the scene tree for the first time.
func _ready():
	## To add in World node
	for yokai in get_tree().get_nodes_in_group("yokai"):
		yokai.connect("dialogue_done", _on_dialogue_done)
	inventaire.give("fleur")

func _on_dialogue_done():
	player.is_talking = false
	## To add in World node
	summoning.revoke_all()
