extends Node2D

@export var texture: Texture2D
@onready var spirit = $Spirit
@onready var animation_player = $AnimationPlayer

var diaglogs = []
signal dialogue_done

func _ready():
	if texture != null:
		spirit.texture = texture
	for dialogue in get_children():
		if dialogue.is_in_group("dialogue"):
			dialogue.connect("done", _on_dialogue_done)
			diaglogs.append(dialogue)

func summon_by(player):
	animation_player.play("appear")
	diaglogs[0].play()
	player.is_talking = true

func _on_dialogue_done():
	emit_signal("dialogue_done")

func revoke():
	if spirit.visible:
		animation_player.play("disapear")

func _on_animation_player_animation_finished(anim_name):
	if anim_name == "appear":
		animation_player.play("idle")
