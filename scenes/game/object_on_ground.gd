extends Node2D

@onready var object = $Object

@export var object_name: String
@export var texture: Texture2D

# Called when the node enters the scene tree for the first time.
func _ready():
	object.texture = texture
