extends Node

var milestones = {
	"livre": false,
	"hache": false,
	"corde": false,
	"fleur": false,
	"jouet": false,
	"bandeau": false,
	"CHIEF_KNEW": false,
	"FORESTER_KNEW": false,
	"AXE_KNEW": false,
	"TOMB_KNEW": false
}

func reset_milestones():
	milestones = {
		"livre": false,
		"hache": false,
		"corde": false,
		"fleur": false,
		"jouet": false,
		"bandeau": false,
		"CHIEF_KNEW": false,
		"FORESTER_KNEW": false,
		"AXE_KNEW": false,
		"TOMB_KNEW": false
	}
