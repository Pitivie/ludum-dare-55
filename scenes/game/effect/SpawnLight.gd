extends Node2D

@export var number_of_ray = 10
var light = preload("res://scenes/game/effect/point_light_2d.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in number_of_ray:
		var ray = light.instantiate()
		ray.position = Vector2(randi()%2000, randi()%2000)
		add_child(ray)
