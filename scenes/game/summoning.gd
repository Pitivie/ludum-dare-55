extends Node

@onready var inventaire = $Inventaire
@export var player: CharacterBody2D

var yokais = []
var links = {
	"fleur": "YOKAI_EMIKO",
	"jouet": "YOKAI_CHILDS",
	"livre": "YOKAI_HIROSHI",
	"bandeau": "YOKAI_KENJIRO"
}

func _ready():
	yokais = get_parent().get_tree().get_nodes_in_group("yokai")

func _on_player_summoning_entered(yokai):
	## yokai var is a ref, not a string and find_key allow only String
	var object = links.find_key(yokai.substr(0))
	inventaire.glow(object)

func _on_inventaire_used_object(object_name):
	inventaire.release_focus()
	var yokai_name = links[object_name]
	for yokai in yokais:
		if yokai.name == yokai_name:
			yokai.summon_by(player)

func _on_player_summoning_exited(yokai):
	## yokai var is a ref, not a string and find_key allow only String
	var object = links.find_key(yokai.substr(0))
	inventaire.mat(object)

func revoke_all():
	for yokai in yokais:
		yokai.revoke()

func _on_player_pick_up_object(object):
	Globals.milestones[object] = true
	inventaire.give(object)
