extends Node2D

@export var texture: Texture2D
@onready var body = $StaticBody2D/Body

signal dialogue_done

func _ready():
	body.texture = texture
	for dialogue in get_children():
		if dialogue.is_in_group("dialogue"):
			dialogue.connect("done", _on_dialogue_done)

func talk():
	for dialogue in get_children():
		if dialogue.is_in_group("dialogue"):
			dialogue.play()
			return

func _on_dialogue_done():
	emit_signal("dialogue_done")
