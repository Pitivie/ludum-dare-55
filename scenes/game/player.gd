extends CharacterBody2D

@onready var talk_info = $TalkInfo
@onready var grab_info = $GrabInfo
@onready var hero = $Hero

const SPEED = 300.0

var talk_to
var grab_to
var is_talking = false

signal summoning_entered
signal summoning_exited
signal pick_up_object

func _physics_process(delta):
	if is_talking:
		return

	if Input.is_action_just_pressed("interact") && talk_to != null:
		print(talk_to)
		talk_to.talk()
		is_talking = true
		return

	if Input.is_action_just_pressed("interact") && grab_to != null:
		emit_signal("pick_up_object", grab_to.object_name)
		grab_to.queue_free()
		return

	var input_direction = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	velocity = input_direction * SPEED
	if velocity.x < 0:
		hero.flip_h = false
	elif velocity.x > 0:
		hero.flip_h = true
	move_and_slide()

func _on_can_talk_body_entered(body):
	if body.get_parent().is_in_group("villager"):
		talk_info.visible = true
		talk_to = body.get_parent()

func _on_can_talk_body_exited(body):
	if body.get_parent().is_in_group("villager"):
		talk_info.visible = false
		talk_to = null

func _on_can_talk_area_entered(area):
	var parent = area.get_parent()
	if parent.is_in_group("yokai"):
		emit_signal("summoning_entered", area.get_parent().name)
	elif parent.is_in_group("object_on_ground"):
		grab_to = parent
		print(grab_to.object_name)
		grab_info.text = "GRAB_" + grab_to.object_name.to_upper()
		grab_info.visible = true

func _on_can_talk_area_exited(area):
	var parent = area.get_parent()
	if parent.is_in_group("yokai"):
		emit_signal("summoning_exited", area.get_parent().name)
	elif parent.is_in_group("object_on_ground"):
		grab_info.visible = false
		grab_to = null
