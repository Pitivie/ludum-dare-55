extends Node2D

@onready var player = $Ground/Player
@onready var inventaire = $Summoning/Inventaire
@onready var summoning = $Summoning
@onready var villager_intro = $Ground/VillagerIntro
@onready var animation_player = $Ground/VillagerIntro/AnimationPlayer
@onready var trigger_intro = $TriggerIntro
@onready var dialogue_tuto_summon = $TriggerTutoSummon/DialogueTutoSummon1
@onready var cercle_object = $CercleObject
@onready var cercle_summon = $CercleSummon
@onready var trigger_tuto_summon = $TriggerTutoSummon
@onready var villager_livre = $Ground/VillagerLivre

signal victory
signal defeat

func _on_dialogue_done():
	player.is_talking = false
	summoning.revoke_all()

func _ready():
	for villager in get_tree().get_nodes_in_group("villager"):
		villager.connect("dialogue_done", _on_dialogue_done)
	for yokai in get_tree().get_nodes_in_group("yokai"):
		yokai.connect("dialogue_done", _on_dialogue_done)

func _on_give_flower():
	Globals.milestones["fleur"] = true
	inventaire.give("fleur")


func _on_CHIEF_KNEW():
	Globals.milestones["CHIEF_KNEW"] = true


func _on_FORESTER_KNEW():
	Globals.milestones["FORESTER_KNEW"] = true


func _on_TOMB_KNEW():
	Globals.milestones["TOMB_KNEW"] = true


func _on_get_axe():
	Globals.milestones["hache"] = true
	inventaire.give("hache")

func _on_AXE_KNEW():
	Globals.milestones["AXE_KNEW"] = true

## Intro
func _on_trigger_intro_body_entered(body):
	villager_intro.talk()
	player.is_talking = true
	trigger_intro.queue_free()

func _on_dialogue_intro_done():
	animation_player.play("go_away")

func _on_animation_player_animation_finished(anim_name):
	villager_intro.queue_free()

func _on_trigger_tuto_summon_body_entered(body):
	dialogue_tuto_summon.play()
	cercle_object.visible = true
	player.is_talking = true

func _on_dialogue_tuto_summon_2_appeared():
	if cercle_object == null:
		return
	if not dialogue_tuto_summon.visible:
		return
	cercle_object.visible = false
	cercle_summon.visible = true

func _on_dialogue_tuto_summon_2_done():
	cercle_summon.visible = false
	trigger_tuto_summon.queue_free()
	player.is_talking = false

func _on_victory_pressed():
	emit_signal("victory")

func _on_defeat_pressed():
	emit_signal("defeat")

func _on_victoire_done():
	emit_signal("victory")

func _on_defaite_done():
	emit_signal("defeat")

func _on_dialogue_book_done():
	Globals.milestones["livre"] = true
	inventaire.give("livre")
	villager_livre.queue_free()
