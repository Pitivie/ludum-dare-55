extends CanvasLayer

@onready var backpack = $Backpack
@onready var object_1 = $Backpack/Object1
@onready var object_2 = $Backpack/Object2
@onready var object_3 = $Backpack/Object3
@onready var object_4 = $Backpack/Object4
@onready var object_5 = $Backpack/Object5
@onready var object_6 = $Backpack/Object6
@onready var hover = $Hover
@onready var smoke = $Smoke

signal used_object

# Called when the node enters the scene tree for the first time.
func _ready():
	for object in backpack.get_children():
		object.visible = false

	object_1.set_meta("object_name", "livre")
	object_2.set_meta("object_name", "hache")
	object_3.set_meta("object_name", "corde")
	object_4.set_meta("object_name", "fleur")
	object_5.set_meta("object_name", "jouet")
	object_6.set_meta("object_name", "bandeau")

func release_focus():
	object_1.release_focus()
	object_2.release_focus()
	object_3.release_focus()
	object_4.release_focus()
	object_5.release_focus()
	object_6.release_focus()

func glow(object_name: String):
	if Globals.milestones[object_name]:
		hover.play(object_name)

func mat(object_name: String):
	hover.stop()
	smoke.visible = false

func give(object_name: String):
	for object in backpack.get_children():
		if object.get_meta("object_name") == object_name:
			object.visible = true

func lost(object_name: String):
	for object in backpack.get_children():
		if object.get_meta("object_name") == object_name:
			object.visible = false

func _on_object_1_pressed():
	## Only when item glow
	if hover.current_animation == object_1.get_meta("object_name"):
		emit_signal("used_object", object_1.get_meta("object_name"))

func _on_object_2_pressed():
	## Only when item glow
	if hover.current_animation == object_2.get_meta("object_name"):
		emit_signal("used_object", object_2.get_meta("object_name"))

func _on_object_3_pressed():
	## Only when item glow
	if hover.current_animation == object_3.get_meta("object_name"):
		emit_signal("used_object", object_3.get_meta("object_name"))

func _on_object_4_pressed():
	## Only when item glow
	if hover.current_animation == object_4.get_meta("object_name"):
		emit_signal("used_object", object_4.get_meta("object_name"))

func _on_object_5_pressed():
	## Only when item glow
	if hover.current_animation == object_5.get_meta("object_name"):
		emit_signal("used_object", object_5.get_meta("object_name"))

func _on_object_6_pressed():
	## Only when item glow
	if hover.current_animation == object_6.get_meta("object_name"):
		emit_signal("used_object", object_6.get_meta("object_name"))
