extends Control

@onready var sparkling_top = $Logo/SparklingTop
@onready var audio_stream_player = $Logo/SparklingTop/AudioStreamPlayer
@onready var sparkling_bottom = $Logo/SparklingBottom
@onready var audio_stream_player_2 = $Logo/SparklingBottom/AudioStreamPlayer2
@onready var animation_player = $AnimationPlayer

func _on_animation_player_animation_finished(anim_name):
	if anim_name == "intro":
		sparkling_top.play("default")
		audio_stream_player.play()
	elif anim_name == "outro":
		queue_free()

func _on_sparkling_top_animation_finished():
	sparkling_bottom.play("default")
	audio_stream_player_2.play()

func _on_sparkling_bottom_animation_finished():
	animation_player.play("outro")
