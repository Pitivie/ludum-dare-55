extends CanvasLayer

signal play_again_requested

func _on_again_pressed():
	emit_signal("play_again_requested")
