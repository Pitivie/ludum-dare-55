extends Control

@onready var credits = $Credits
@onready var controls = $Controls

signal start_game_requested

func _on_play_pressed():
	emit_signal("start_game_requested")

func _on_credit_pressed():
	credits.visible = true

func _on_back_pressed():
	credits.visible = false

func _on_controls_pressed():
	controls.visible = true

func _on_back_controls_pressed():
	controls.visible = false
