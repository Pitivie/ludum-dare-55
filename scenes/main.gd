extends Node

@onready var main_menu = $MainMenu
@onready var defeat = $Defeat
@onready var victory = $Victory

const WORLD = preload("res://scenes/game/world.tscn")
var world

func _on_main_menu_start_game_requested():
	world = WORLD.instantiate()
	world.connect("victory", _on_world_victory)
	world.connect("defeat", _on_world_defeat)
	add_child(world)
	move_child(world, 0)
	main_menu.visible = false

func _on_world_victory():
	world.visible = false
	victory.visible = true

func _on_world_defeat():
	world.visible = false
	defeat.visible = true

func _on_defeat_play_again_requested():
	defeat.visible = false
	main_menu.visible = true
	world.queue_free()
	Globals.reset_milestones()

func _on_victory_play_again_requested():
	victory.visible = false
	main_menu.visible = true
	world.queue_free()
	Globals.reset_milestones()
