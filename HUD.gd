extends CanvasLayer

const EN = preload("res://assets/sprites/menu/EN.png")
const FR = preload("res://assets/sprites/menu/FR.png")
@onready var toggle_langue = $ToggleLangue
@onready var volume = $Volume

func _ready():
	if TranslationServer.get_locale() == "en":
		toggle_langue.texture_normal = FR
	else:
		toggle_langue.texture_normal = EN

func _on_toggle_langue_pressed():
	if TranslationServer.get_locale() == "en":
		TranslationServer.set_locale("fr")
		toggle_langue.texture_normal = EN
	else:
		TranslationServer.set_locale("en")
		toggle_langue.texture_normal = FR
	toggle_langue.release_focus()

func _on_toggle_sound_pressed():
	var bus_index = AudioServer.get_bus_index("Master")
	AudioServer.set_bus_mute(bus_index, not AudioServer.is_bus_mute(bus_index))

func _on_h_slider_drag_ended(value_changed):
	AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Master"), linear_to_db(volume.value)-20)
